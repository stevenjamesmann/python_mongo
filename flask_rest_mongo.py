from flask import Flask, jsonify, request, Response
from flask_restful import Resource, Api
import dao

app = Flask(__name__)
api = Api(app)


class CustomerList(Resource):
    def get(self):
        return dao.find_all_customers()

    def post(self):
        data = request.get_json(force=True)
        customer_name = data['name']
        customer_address = data['address']
        dao.insert_single_customer(customer_name, customer_address)
        return jsonify(name=customer_name, address=customer_address)


class Customer(Resource):
    def get(self, customer_id):
        customer = dao.find_single_customer(customer_id)
        return customer

    def delete(self, customer_id):
        dao.delete_single_customer(customer_id)
        return 'Customer Deleted', 200

    def put(self, customer_id):
        data = request.get_json(force=True)
        customer_name = data['name']
        customer_address = data['address']

        dao.update_single_customer(customer_id, customer_name, customer_address)
        return jsonify(id=customer_id, name=customer_name, address=customer_address)


api.add_resource(CustomerList, '/customers')
api.add_resource(Customer, '/customers/<int:customer_id>')

if __name__ == '__main__':
    app.run(debug=True)
