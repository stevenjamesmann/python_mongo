import pymongo
from bson.json_util import dumps

client = pymongo.MongoClient("mongodb://localhost:27017/")
database = client["mydatabase"]
collection = database["customers"]

# TODO Single Account insertion
# account_dictionary = {"account_name":{"0":"mang0thegoat"},"private":{"0":0},"verified":{"0":0},"biography":{"0":"Joseph Marquez\nMelee player , beer drinker , tattoos, America and daddy"},"posts":{"0":118},"follows":{"0":113},"followers":{"0":15400},"profile_img":{"0":"https:\/\/scontent-yyz1-1.cdninstagram.com\/vp\/11675dbd82458a62760928e3fde7250a\/5D1E6F45\/t51.2885-19\/11887215_774818445973303_663405003_a.jpg?_nc_ht=scontent-yyz1-1.cdninstagram.com"},"total_comments":{"0":1095},"analysis_time":{"0":"0:44:28"}}
# collection.insert_one(account_dictionary)

# TODO inserts one single record into the collection
# customer_dictionary = { "name": "John", "address": "123 Fake Street"}
# x = collection.insert_one(customer_dictionary)

# TODO inserts multiple records into the collection
# customer_list = [
#   { "name": "Amy", "address": "Apple st 652"},
#   { "name": "Hannah", "address": "Mountain 21"},
#   { "name": "Michael", "address": "Valley 345"},
#   { "name": "Sandy", "address": "Ocean blvd 2"},
#   { "name": "Betty", "address": "Green Grass 1"},
#   { "name": "Richard", "address": "Sky st 331"},
#   { "name": "Susan", "address": "One way 98"},
#   { "name": "Vicky", "address": "Yellow Garden 2"},
#   { "name": "Ben", "address": "Park Lane 38"},
#   { "name": "William", "address": "Central st 954"},
#   { "name": "Chuck", "address": "Main Road 989"},
#   { "name": "Viola", "address": "Sideway 1633"}
# ]
# x = collection.insert_many(customer_list)

# TODO insert multiple records into the collection with pre-defined IDs
# mylist = [
#   { "_id": 1, "name": "John", "address": "Highway 37"},
#   { "_id": 2, "name": "Peter", "address": "Lowstreet 27"},
#   { "_id": 3, "name": "Amy", "address": "Apple st 652"},
#   { "_id": 4, "name": "Hannah", "address": "Mountain 21"},
#   { "_id": 5, "name": "Michael", "address": "Valley 345"},
#   { "_id": 6, "name": "Sandy", "address": "Ocean blvd 2"},
#   { "_id": 7, "name": "Betty", "address": "Green Grass 1"},
#   { "_id": 8, "name": "Richard", "address": "Sky st 331"},
#   { "_id": 9, "name": "Susan", "address": "One way 98"},
#   { "_id": 10, "name": "Vicky", "address": "Yellow Garden 2"},
#   { "_id": 11, "name": "Ben", "address": "Park Lane 38"},
#   { "_id": 12, "name": "William", "address": "Central st 954"},
#   { "_id": 13, "name": "Chuck", "address": "Main Road 989"},
#   { "_id": 14, "name": "Viola", "address": "Sideway 1633"}
# ]
#
# x = collection.insert_many(mylist)

# TODO finds the first record within the collection
# x = collection.find_one()

# TODO finds all records within the collection
# records = collection.find()
# for record in records:
#     print(record)

# TODO finds all records within the collection but only returns specified keys
# for x in collection.find({},{ "_id": 0, "name": 1, "address": 1 }):
#   print(x)

# TODO finds a record within the collection that matches the query
# customer_query = { "address": "Park Lane 38" }
#
# customer_document = collection.find(customer_query)
#
# for customer in customer_document:
#     print(customer)


def find_all_customers():
    customers = collection.find()
    customer_data = dumps(customers)
    return customer_data


def find_single_customer(customer_id):
    customer = collection.find_one(customer_id)
    return dumps(customer)


def insert_single_customer(customer_name, customer_address):
    customer_dict = {"name": customer_name, "address": customer_address}
    collection.insert_one(customer_dict)


def delete_single_customer(customer_id):
    customer_dict = {"_id": f"{customer_id}"}
    collection.delete_one(customer_dict)


def update_single_customer(customer_id, customer_name, customer_address):
    customer_dict = {"name": customer_name, "address": customer_address}
    collection.update_one({"_id": customer_id}, {"$set": customer_dict})
